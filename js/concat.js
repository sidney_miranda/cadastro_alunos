var alunos = [];

document.querySelector("#cadastrar").addEventListener('click', () => {
    alunos.push({
        'nome': document.querySelector('#nomeAluno').value,
        'sobrenome': document.querySelector('#sobrenomeAluno').value,
        'dob': document.querySelector('#dtNascimento').value.split('-').reverse().join('-'),
        'serie': document.querySelector('input[list]').value,
        'sexo': document.querySelector('input[name=sexo]:checked').value,
        'cep': document.querySelector('#cep').value,
        'rua': document.querySelector('#rua').value,
        'bairro': document.querySelector('#bairro').value,
        'cidade': document.querySelector('#cidade').value,
        'estado': document.querySelector('#estado').value,
        'email': document.querySelector('#email').value,
        'mae': document.querySelector('#nomeMae').value,
        'pai': document.querySelector('#nomePai').value

    });
    console.log(alunos)
});

document.querySelector('#cep').addEventListener('input', () => {
    const cep = document.querySelector('#cep').value;

    fetch(`https://viacep.com.br/ws/${cep}/json`)
        .then(resp => resp.json())
        .then(data => {
            document.querySelector('#rua').value = data.logradouro;
            document.querySelector('#bairro').value = data.bairro;
            document.querySelector('#cidade').value = data.localidade;
            document.querySelector('#estado').value = data.uf;
        })
});


document.querySelector('#cadastrados').addEventListener('click', () => {
    var tbody = document.querySelector('#tbody');
    tbody.innerHTML = "";

    alunos.forEach(aluno => {
        let tr = createElement('tr');

        let td_nome = createElement('td');
        let td_sobrenome = createElement('td');
        let td_dob = createElement('td');
        let td_serie = createElement('td');
        let td_sexo = createElement('td');
        let td_cep = createElement('td');
        let td_rua = createElement('td');
        let td_bairro = createElement('td');
        let td_cidade = createElement('td');
        let td_estado = createElement('td');
        let td_email = createElement('td');
        let td_mae = createElement('td');
        let td_pai = createElement('td');

        td_nome.textContent = aluno.nome;
        td_sobrenome.textContent = aluno.sobrenome;
        td_dob.textContent = aluno.dob;
        td_serie.textContent = aluno.serie;
        td_sexo.textContent = aluno.sexo;
        td_cep.textContent = aluno.cep;
        td_rua.textContent = aluno.rua;
        td_bairro.textContent = aluno.bairro;
        td_cidade.textContent = aluno.cidade;
        td_estado.textContent = aluno.estado;
        td_email.textContent = aluno.email;
        td_mae.textContent = aluno.mae;
        td_pai.textContent = aluno.pai;

        tr.append(td_nome);
        tr.append(td_sobrenome);
        tr.append(td_dob);
        tr.append(td_serie);
        tr.append(td_sexo);
        tr.append(td_cep);
        tr.append(td_rua);
        tr.append(td_bairro);
        tr.append(td_cidade);
        tr.append(td_estado);
        tr.append(td_email);
        tr.append(td_mae);
        tr.append(td_pai);

        tbody.appendChild(tr);
    })
});

function createElement(el) {
    return document.createElement(el);
}